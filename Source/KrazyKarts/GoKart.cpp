// Copyright © 2021 Derek Fletcher, All rights reserved

#include "GoKart.h"

#include "GoKartMovementComponent.h"

#include "DrawDebugHelpers.h"

FString GetEnumText(const ENetRole Role)
{
	switch (Role)
	{
	case ROLE_None: return "None";
	case ROLE_SimulatedProxy: return "SimulatedProxy";
	case ROLE_AutonomousProxy: return "AutonomousProxy";
	case ROLE_Authority: return "Authority";
	case ROLE_MAX: return "MAX";
	default: return "ERROR";
	}
}

AGoKart::AGoKart()
{
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);
	SetReplicatingMovement(false);

	MovementComponent = CreateDefaultSubobject<UGoKartMovementComponent>(TEXT("MovementComponent"));
}

void AGoKart::BeginPlay()
{
	Super::BeginPlay();
}

void AGoKart::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGoKart::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGoKart::MoveRight);
}

void AGoKart::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	DrawDebugString(GetWorld(), FVector(0, 0, 100), GetEnumText(GetLocalRole()), this, FColor::White, DeltaTime);
}

void AGoKart::MoveForward(const float Value)
{
	if (MovementComponent)
	{
		MovementComponent->SetThrottle(Value);
	}
}

void AGoKart::MoveRight(const float Value)
{
	if (MovementComponent)
	{
		MovementComponent->SetSteeringThrow(Value);
	}
}
