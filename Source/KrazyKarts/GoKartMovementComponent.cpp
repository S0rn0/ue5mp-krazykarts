// Copyright © 2021 Derek Fletcher, All rights reserved


#include "GoKartMovementComponent.h"
#include "GameFramework/GameStateBase.h"
#include "Net/UnrealNetwork.h"

UGoKartMovementComponent::UGoKartMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}

void UGoKartMovementComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

void UGoKartMovementComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UGoKartMovementComponent, ServerState);
}

void UGoKartMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	APawn* Owner = Cast<APawn>(GetOwner());
	if (Owner->IsLocallyControlled())
	{
		const FGoKartMove Move = CreateMove(DeltaTime);
		if (!Owner->HasAuthority())
		{
			UnacknowledgedMoves.Add(Move);
			SimulateMove(Move);
		}
		Server_SendMove(Move);
	}

	if (GetOwnerRole() == ROLE_SimulatedProxy)
	{
		SimulateMove(ServerState.LastMove);
	}
}

FGoKartMove UGoKartMovementComponent::CreateMove(const float DeltaTime) const
{
	FGoKartMove Move;
	Move.DeltaTime = DeltaTime;
	Move.Throttle = Throttle;
	Move.SteeringThrow = SteeringThrow;
	Move.Time = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();

	return Move;
}

void UGoKartMovementComponent::SimulateMove(const FGoKartMove& Move)
{
	UpdateVelocity(Move);
	UpdateRotation(Move);
	ApplyVelocityAndRotation(Move);
}

void UGoKartMovementComponent::UpdateVelocity(const FGoKartMove& Move)
{
	// Force from throttle input
	const FVector ThrottleForce = Move.Throttle * MaxDrivingForce * GetOwner()->GetActorForwardVector();

	// Force from air resistance
	const FVector AirResistanceForce = -Velocity.SizeSquared() * Velocity.GetSafeNormal() * DragCoefficient;

	// Force from rolling resistance
	const FVector RollingResistanceForce = Velocity.GetSafeNormal() * (Mass * (GetWorld()->GetGravityZ() / 100.f)) * RollingResistanceCoefficient ; // Convert CM to M;
	
	const FVector Acceleration = (ThrottleForce + AirResistanceForce + RollingResistanceForce) / Mass;
	Velocity += Acceleration * Move.DeltaTime;

	Velocity = Rotation.RotateVector(Velocity);
}

void UGoKartMovementComponent::UpdateRotation(const FGoKartMove& Move)
{
	const float RotationAngle = FVector::DotProduct(GetOwner()->GetActorForwardVector(), Velocity) * Move.DeltaTime / MinTurningRadius * Move.SteeringThrow;
	Rotation = FQuat(GetOwner()->GetActorUpVector(), RotationAngle);
}

void UGoKartMovementComponent::ApplyVelocityAndRotation(const FGoKartMove& Move)
{
	GetOwner()->AddActorWorldRotation(Rotation);
	
	const FVector Translation = Velocity * Move.DeltaTime * 100; // Convert CM to M

	FHitResult SweepHitResult;
	GetOwner()->AddActorWorldOffset(Translation, true, OUT &SweepHitResult);
	if (SweepHitResult.IsValidBlockingHit())
	{
		Velocity = FVector::ZeroVector;
	}
}

void UGoKartMovementComponent::OnRep_ServerState()
{
	GetOwner()->SetActorTransform(ServerState.Transform);
	Velocity = ServerState.Velocity;
	Rotation = ServerState.Rotation;

	TArray<FGoKartMove> NewMoves;
	for (const FGoKartMove& Move : UnacknowledgedMoves)
	{
		if (Move.Time > ServerState.LastMove.Time)
		{
			NewMoves.Add(Move);
		}
	}
	UnacknowledgedMoves = NewMoves;

	for (const FGoKartMove& Move : UnacknowledgedMoves)
	{
		SimulateMove(Move);
	}
}

void UGoKartMovementComponent::Server_SendMove_Implementation(FGoKartMove Move)
{
	SimulateMove(Move);
	
	ServerState.LastMove = Move;
	ServerState.Transform = GetOwner()->GetActorTransform();
	ServerState.Velocity = Velocity;
	ServerState.Rotation = Rotation;
}

bool UGoKartMovementComponent::Server_SendMove_Validate(FGoKartMove Move)
{
	return true;
}
