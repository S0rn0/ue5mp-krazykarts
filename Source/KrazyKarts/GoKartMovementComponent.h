// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.generated.h"

USTRUCT()
struct FGoKartMove
{
	GENERATED_BODY()

	UPROPERTY()
	float Throttle;
	
	UPROPERTY()
	float SteeringThrow;

	UPROPERTY()
	float DeltaTime;

	UPROPERTY()
	float Time;
};

USTRUCT()
struct FGoKartState
{
	GENERATED_BODY()

	UPROPERTY()
	FVector Velocity;

	UPROPERTY()
	FQuat Rotation;

	UPROPERTY()
	FTransform Transform;

	UPROPERTY()
	FGoKartMove LastMove;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class KRAZYKARTS_API UGoKartMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UGoKartMovementComponent();

	FORCEINLINE void SetThrottle(const float Value) { Throttle = Value; }
	FORCEINLINE void SetSteeringThrow(const float Value) { SteeringThrow = Value; }
	
protected:
	virtual void BeginPlay() override;

private:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FGoKartMove CreateMove(float DeltaTime) const;
	void SimulateMove(const FGoKartMove& Move);

	// Mass of the Go Kart (KG)
	UPROPERTY(EditAnywhere)
	float Mass = 1000.f;

	// Force applied to Go Kart when throttle is full (N)
	UPROPERTY(EditAnywhere)
	float MaxDrivingForce = 10000.f;

	// Minimum radius of the car turning circle (M)
	UPROPERTY(EditAnywhere)
	float MinTurningRadius = 10.f;

	// Larger the value the more air resistance on the Go Kart
	UPROPERTY(EditAnywhere)
	float DragCoefficient = 16.f;

	// Larger the value the more rolling resistance on the Go Kart
	UPROPERTY(EditAnywhere)
	float RollingResistanceCoefficient = 0.015f;

	UPROPERTY(ReplicatedUsing=OnRep_ServerState)
	FGoKartState ServerState;

	TArray<FGoKartMove> UnacknowledgedMoves;
	
	FVector Velocity;
	FQuat Rotation;

	float Throttle = 0.f;
	float SteeringThrow = 0.f;

	void UpdateVelocity(const FGoKartMove& Move);
	void UpdateRotation(const FGoKartMove& Move);
	void ApplyVelocityAndRotation(const FGoKartMove& Move);
	
	UFUNCTION()
	void OnRep_ServerState();

	UFUNCTION(Server, Unreliable, WithValidation)
	void Server_SendMove(FGoKartMove Move);
};
