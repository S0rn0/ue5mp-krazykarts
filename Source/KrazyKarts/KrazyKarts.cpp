// Copyright © 2021 Derek Fletcher, All rights reserved

#include "KrazyKarts.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, KrazyKarts, "KrazyKarts" );
