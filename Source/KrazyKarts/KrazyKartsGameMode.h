// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KrazyKartsGameMode.generated.h"

UCLASS()
class KRAZYKARTS_API AKrazyKartsGameMode final : public AGameModeBase
{
	GENERATED_BODY()
	
};
